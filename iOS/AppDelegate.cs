﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace BloomFilterReader.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			var sqlitePlatform = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS ();
			string databasePath = System.IO.Path.Combine (
				                      Environment.GetFolderPath (Environment.SpecialFolder.Personal),
				                      "bloomfilters.db3");
			var db = new SQLite.Net.SQLiteConnection (sqlitePlatform, databasePath);

			LoadApplication (new App (db));

			return base.FinishedLaunching (app, options);
		}
	}
}

