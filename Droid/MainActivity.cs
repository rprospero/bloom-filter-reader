﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace BloomFilterReader.Droid
{
	[Activity (Label = "BloomFilterReader.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);


			var sqlitePlatform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();
			string databasePath = System.IO.Path.Combine (
				System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal),
				"bloomfilters.db3");
			var db = new SQLite.Net.SQLiteConnection (sqlitePlatform, databasePath);

			LoadApplication (new App (db));
		}
	}
}

