﻿using System;

using Xamarin.Forms;

namespace BloomFilterReader
{
	public class App : Application
	{
		public App (SQLite.Net.SQLiteConnection db)
		{
			MainPage = new MyPage (db);
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

