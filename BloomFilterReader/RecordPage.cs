﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using Xamarin.Forms;

namespace BloomFilterReader
{
    public class RecordPage : ContentPage
    {
		private ObservableCollection<List<string>> fieldValues;
		private BloomFilterEntry bfe;
		private SQLite.Net.SQLiteConnection db;
		private CancellationTokenSource ts;

		public RecordPage(SQLite.Net.SQLiteConnection jointdb, BloomFilterEntry sharedBfe)
        {
            Content = new StackLayout();
            Title = "Records";
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);
			bfe = sharedBfe;
			db = jointdb;
			fieldValues = new ObservableCollection<List<string>>();
			ts = new CancellationTokenSource();
        }

        protected override void OnAppearing() {
			/*var json = @"[
                {'kind':'Entry', 'name':'Name'},
                {'kind':'Options', 'name':'Gender', 'options':['Female','Male']},
                {'kind':'Numbers', 'name':'Year', 'low':1928, 'high':1945}]";*/

			int row = 0;
			var layout = new Grid {
				ColumnDefinitions = {
					new ColumnDefinition { Width = GridLength.Auto },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)},
					new ColumnDefinition { Width = GridLength.Auto }
				}
			};
			var rowDefs = new RowDefinitionCollection();
			string json = bfe.MetaData;
			if (json == null) {
				layout.Children.Add(new Label { Text = "No data available" }, 0, 0);
				Content = layout;
				return;
			}
			JArray m = JArray.Parse(json);

            foreach(JObject meta in m) {
				if (fieldValues.Count <= row) {
					fieldValues.Add(new List<string>());
				}
				fieldGenerator(meta,layout,row);
				rowDefs.Add(new RowDefinition { Height = GridLength.Auto });
				row += 1;
            }
			while (row < fieldValues.Count) {
				fieldValues.RemoveAt(row);
			}
			rowDefs.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
			rowDefs.Add(new RowDefinition { Height = GridLength.Auto });
			rowDefs.Add(new RowDefinition { Height = GridLength.Auto });
			rowDefs.Add(new RowDefinition { Height = GridLength.Auto });

			layout.RowDefinitions = rowDefs;

			var resultView = new TableView();
			var insertButton = new Button { Text = "Insert" };

			insertButton.Clicked += (sender, e) => {
				List<string> records = getRecord();
				if (records.Count != 1) {
					DisplayAlert("Insert Error", "Cannot insert records while search switches are active.", "OK");
					return;
				}
				string record = records.First().Substring(1);
				var bf = bfe.getBloomFilter();
				bf.insert(record);
				bf.updateEntry(bfe);
				db.Update(bfe);
				fieldValues.Move(0, 0);
			};

			layout.Children.Add(resultView, 0, 3, row, row+1);
			layout.Children.Add(insertButton, 0, 3, row + 1, row + 2);

			var progress = new ProgressBar();
			progress.Progress = 0;
			layout.Children.Add(progress, 0, 3, row + 2, row + 3);

            Content = layout;

			fieldValues.CollectionChanged += async (sender, e) => {
				var bf = bfe.getBloomFilter();
				List<string> record = getRecord();

				ts.Cancel();
				ts = new CancellationTokenSource();
				var tstemp = ts;
				var results = await Task.Run(
					() => {
						List<String> filtered = new List<String>();
						double index = 0;
						foreach (String r in record) {
							Device.BeginInvokeOnMainThread(() => progress.Progress = index/record.Count);
							if (bf.contains(r.Substring(1))) {
								filtered.Add(r);
							};
							if (tstemp.IsCancellationRequested) {
								return new List<String>();
							}
							index += 1;
						};
						return filtered;
					});

				var table = new TableRoot("Results");
				var sections = results.Select((string arg) => {
					var fields = arg.Substring(1).Split("\x1E".ToCharArray(), m.Count);
					var section = new TableSection("Match");
					int i = 0;
					foreach (string f in fields) {
						section.Add(new TextCell {
							Detail = (string)m[i]["label"],
							Text = f
						});
						i += 1;
					}

					return section;
					});
				table.Add(sections);
				resultView.Root = table;
				progress.Progress = 1;
			};
        }

		private List<string> getRecord() {
			/*string record = fieldValues[0];
			foreach (string s in fieldValues.Skip(1)) {
				record += "\x1E" + s;
			}
			return record;*/
			List<string> result = new List<string>();
			result.Add("");

			System.Diagnostics.Debug.WriteLine(fieldValues.Aggregate("",(arg1, arg2) => arg1+arg2.Aggregate("",(arg3, arg4) => arg3+arg4)));
			System.Diagnostics.Debug.WriteLine(fieldValues.Count.ToString());
			foreach (List<string> r in fieldValues) {
				List<string> temp = new List<string>();
				foreach (string s in r) {
					foreach (string v in result) {
						temp.Add(v + "\x1E" + s);
					}
				}
				result = temp;
			}
			return result;
		}

        private async Task<string> getJSON()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync("http://rprosperowork.duckdns.org:8000/example.bloom.json");
            //response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

		private void fieldGenerator(JObject meta, Grid g, int row) {
            g.Children.Add(new Label { Text = (string)meta["label"] }, 0, row);
			List<string> temp;
            switch ((string)meta["kind"]) {
                case "Option":
                    var picker = new Picker();
					var seeker = new Switch();

                    foreach(string option in meta["options"])
                    {
                        picker.Items.Add(option);
                    }

					System.EventHandler optionHandler = (object sender, EventArgs e) => {
						if (!seeker.IsToggled) {
							temp = new List<string>();
							temp.Add((string)meta["options"][picker.SelectedIndex]);
							fieldValues[row] = temp;
						} else {
							fieldValues[row] = meta["options"].Select(x => (string) x).ToList();
						}
					};
					System.EventHandler<Xamarin.Forms.ToggledEventArgs> optionToggleHandler = (object sender, ToggledEventArgs e) => {
						if (!seeker.IsToggled) {
							temp = new List<string>();
							temp.Add((string)meta["options"][picker.SelectedIndex]);
							fieldValues[row] = temp;
						} else {
							fieldValues[row] = meta["options"].Select(x => (string)x).ToList();
						}
					};
					picker.SelectedIndexChanged += optionHandler;
					seeker.Toggled += optionToggleHandler;


					if (fieldValues.Count > row &&
						fieldValues[row].Count > 0 &&
						picker.Items.IndexOf(fieldValues[row].First()) > 0) {
						picker.SelectedIndex = picker.Items.IndexOf(fieldValues[row].First());
					} else {
						picker.SelectedIndex = 0;
					}

					g.Children.Add(picker, 1, row);
					g.Children.Add(seeker, 2, row);
                    break;
                case "Range":
                    picker = new Picker();
					seeker = new Switch();

                    for(int i = (int)meta["low"]; i<= (int)meta["high"]; i += 1) {
                        picker.Items.Add(i.ToString());
                    }

					EventHandler rangeHandler = (sender, e) => {
						if (!seeker.IsToggled) {
							temp = new List<string>();
							temp.Add(((int)meta["low"] + picker.SelectedIndex).ToString());
							fieldValues[row] = temp;
						} else {
							fieldValues[row] = picker.Items.ToList();
						}
					};
					EventHandler<Xamarin.Forms.ToggledEventArgs> rangeToggledHandler = (sender, e) => {
						if (!seeker.IsToggled) {
							temp = new List<string>();
							temp.Add(((int)meta["low"] + picker.SelectedIndex).ToString());
							fieldValues[row] = temp;
						} else {
							fieldValues[row] = picker.Items.ToList();
						}
					};

					picker.SelectedIndexChanged += rangeHandler;
					seeker.Toggled += rangeToggledHandler;

					if (fieldValues.Count > row &&
					    fieldValues[row].Count > 0 &&
					    picker.Items.IndexOf(fieldValues[row].First()) > 0) {
						picker.SelectedIndex = picker.Items.IndexOf(fieldValues[row].First());
					} else {
						picker.SelectedIndex = 0;
					}

					g.Children.Add(picker, 1, row);
					g.Children.Add(seeker, 2, row);
                    break;
                default:
                    Entry entry = new Entry();
					entry.TextChanged += (sender, e) => {
						temp = new List<string>();
						temp.Add(entry.Text.ToLower().Trim());
						fieldValues[row] = temp;
					};

					if (fieldValues.Count > row && fieldValues[row].Count > 0) {
						entry.Text = fieldValues[row].First();
					}

                    g.Children.Add(entry, 1, row);
                    break;
            }
        }
    }
}
