﻿using System;

using Xamarin.Forms;

namespace BloomFilterReader
{
	public class MyPage : TabbedPage
	{
		public MyPage (SQLite.Net.SQLiteConnection db)
		{
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);
			this.Title = "Bloom Filter Reader Main";

			var bfe = new BloomFilterEntry();

			this.Children.Add(new ManagePage(db, bfe));
            this.Children.Add(new RecordPage(db, bfe));
		}
	}
}


