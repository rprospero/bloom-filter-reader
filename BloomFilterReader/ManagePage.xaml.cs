﻿using System;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace BloomFilterReader {
	public partial class ManagePage : ContentPage {
		private BloomFilterEntry bfe;
		private String chosen;
		private SQLite.Net.SQLiteConnection db;
		public ManagePage (SQLite.Net.SQLiteConnection jointdb, BloomFilterEntry jointBfe) {
			InitializeComponent ();
			bfe = jointBfe;
			db = jointdb;

			db.DropTable<BloomFilterEntry> ();
			db.CreateTable<BloomFilterEntry> ();
			updatePicker ();
		}

		private void updatePicker() {
			var bfes = db.Table<BloomFilterEntry> ();
			filter.Items.Clear ();
			foreach (BloomFilterEntry bf in bfes) {
				filter.Items.Add (bf.Name);
			}
		}

		public void onPick(object sender, EventArgs args) {
			if (filter.SelectedIndex < 0) {
				return;
			}
			chosen = filter.Items[filter.SelectedIndex];
			var temp = db.Table<BloomFilterEntry> ().Where (w => w.Name == chosen).First ();
			temp.overWrite(bfe);
		}

		public void onCopy(object sender, EventArgs args) {
			BloomFilterEntry temp = new BloomFilterEntry ();
			temp.Name = input.Text;

			temp.Bits = bfe.Bits;
			temp.Hashes = bfe.Hashes;
			temp.Mask = (byte[]) bfe.Mask.Clone ();
			temp.MetaData = bfe.MetaData;
			db.Insert (temp);
			temp.overWrite(bfe);

			updatePicker ();
			filter.SelectedIndex = filter.Items.IndexOf (bfe.Name);
		}

		public void onDelete(object sender, EventArgs args) {
			db.Delete (bfe);
			updatePicker();
		}

		public void onRename(object Sender, EventArgs args) {
			bfe.Name = input.Text;
			db.Update (bfe);
			updatePicker ();
			input.Text = "";
			filter.SelectedIndex = filter.Items.IndexOf (bfe.Name);
		}

		public async void onDownload(object Sender, EventArgs args) {
			BloomFilter bf;
			try {
				bf = await BloomFilter.fromURI(new Uri(input.Text));
			} catch (Exception e) {
				await DisplayAlert("Download Failed", e.Message, "OK");
				return;
			}

			bf.updateEntry(bfe);
			bfe.Name = input.Text;
			var json = await getJSON(new Uri(input.Text + ".json"));
			bfe.MetaData = json;

			db.Insert(bfe);
			updatePicker();
			input.Text = "";
			filter.SelectedIndex = filter.Items.IndexOf(bfe.Name);
		}
		public async void onUpload(object Sender, EventArgs args) {
			var bf = bfe.getBloomFilter();
			string json = bfe.MetaData;

			Uri destination = new Uri(input.Text);
			var client = new HttpClient();
			var content = new System.Net.Http.ByteArrayContent(bf.toFile());
			try {
				var result = await client.PutAsync(destination, content);
				if (!result.IsSuccessStatusCode) {
					await DisplayAlert("Upload Failed", result.ReasonPhrase, "OK");
					return;
				}
			} catch (Exception e) {
				await DisplayAlert("Upload Failed", e.Message, "OK");
				return;
			}

			destination = new Uri(input.Text + ".json");
			content = new StringContent(json, System.Text.UTF8Encoding.UTF8, "application/json");
			try {
				var result = await client.PutAsync(destination, content);
				if (!result.IsSuccessStatusCode) {
					await DisplayAlert("Upload Failed", result.ReasonPhrase, "OK");
					return;
				}
			} catch (Exception e) {
				await DisplayAlert("Upload Failed", e.Message, "OK");
				return;
			}

			await DisplayAlert("Upload", "Upload successful", "OK");
			input.Text = "";
			return;

		}

		private async void onScan(object Sender, EventArgs args) {
			
		#if __ANDROID__
    		// Initialize the scanner first so it can track the current context
    		MobileBarcodeScanner.Initialize (Application);
		#endif

			var scanner = new ZXing.Mobile.MobileBarcodeScanner();
			var result = await scanner.Scan();

			if (result != null) input.Text = result.Text;
		}

		private async Task<string> getJSON(Uri uri) {
			var httpClient = new HttpClient();
			var response = await httpClient.GetAsync(uri);
			return await response.Content.ReadAsStringAsync();
		}
	}
}