﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using PCLCrypto;
using SQLite.Net;
using SQLite.Net.Attributes;

namespace BloomFilterReader
{
	public class BloomFilter
	{
		private byte[] mask;
		private ulong hashes;
		private ulong bits;

		private ulong bytesToLong(byte[] bytes, int offset) {
			ulong result = 0;
			for (int i = 0; i < 8; i++) {
				result = (result << 8) + bytes [i + offset];
			}
			return result;
		}

		public BloomFilter (ulong bit_count, ulong hash_count) {
			hashes = hash_count;
			bits = bit_count;

			ulong bytes = bits / 8;
			if (bits % 8 != 0) {
				bytes += 1;
			}
			mask = new byte[bytes];
			clear ();
		}

		public BloomFilter (ulong bit_count, ulong hash_count, String s) : this(bit_count, hash_count) {
			insert (s);
		}
			
		public BloomFilter(ulong bit_count, ulong hash_count, byte[] bytes) {
			hashes = hash_count;
			bits = bit_count;
			mask = bytes;
		}

        public static async Task<BloomFilter> fromURI(Uri uri) {
            var httpClient = new HttpClient();
            byte[] result = await httpClient.GetByteArrayAsync(uri);
            byte[] mask = result.Skip(16).ToArray();

			var temp = result.Take(8);
			if (BitConverter.IsLittleEndian) {
				temp = temp.Reverse();
			}
			ulong bits = BitConverter.ToUInt64(temp.ToArray(),0);

			temp = result.Skip(8).Take(8);
			if (BitConverter.IsLittleEndian) {
				temp = temp.Reverse();
			}
			ulong hashes = BitConverter.ToUInt64(temp.ToArray(), 0);
            return new BloomFilter(bits, hashes, mask);
        }

		public void clear() {
			for (int i = 0; i < mask.Length; i++) {
				mask [i] = 0;
			}
		}

		public bool contains(BloomFilter query) {
			if (mask.Length != query.mask.Length) {
				return false;
			}
			for (int i = 0; i < mask.Length; i++) {
				if ((query.mask [i] & ~mask [i]) != 0) {
					return false;
				}
			}
			return true;
		}

		public bool contains(String s) {
			return contains (new BloomFilter (bits, hashes, s));
		}

		public void insert(byte[] raw_bits) {
			var md = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha512);
			byte[] value_bits = md.HashData(raw_bits);

			ulong count = bits;
			for (ulong i = 0; i < hashes; i++) {
				ulong bit = bytesToLong (value_bits, 0) + i * bytesToLong (value_bits, 8);
				bit %= count;
				ulong subbyte = bit >> 3;
				ulong subbit = bit & 0x07;
				mask [subbyte] |= (byte)(((byte)1) << ((byte)subbit));
			}
		}

		public void insert(String s) {
			insert (Encoding.UTF8.GetBytes (s));
		}

		public void updateEntry(BloomFilterEntry bfe) {
			bfe.Hashes = (long)hashes;
			bfe.Bits = (long)bits;
			bfe.Mask = mask;
		}

		public void updateFromEntry(BloomFilterEntry bfe) {
			hashes = (ulong)bfe.Hashes;
			bits = (ulong)bfe.Bits;
			mask = bfe.Mask;
		}

		public byte[] toFile() {
			byte[] output = new byte[mask.Length + 16];
			byte[] temp = BitConverter.GetBytes(bits);
			if (BitConverter.IsLittleEndian) {
				temp = temp.Reverse().ToArray();
			}
			temp.CopyTo(output, 0);
			temp = BitConverter.GetBytes(hashes);
			if (BitConverter.IsLittleEndian) {
				temp = temp.Reverse().ToArray();
			}
			temp.CopyTo(output, 8);
			mask.CopyTo(output, 16);
			return output;
		}
	}

	public class BloomFilterEntry {
		public BloomFilterEntry() {}
		[PrimaryKey, AutoIncrement]
		public long Id {get; set;}

		[Indexed]
		public string Name {get; set;}

		//Sqlite does not support unsigned 64bit integers
		public long Hashes {get; set;}
		public long Bits {get; set;}
		public byte[] Mask { get; set; }
		public string MetaData { get; set;}
		public void overWrite(BloomFilterEntry x) {
			x.Id = Id;
			x.Name = Name;
			x.Hashes = Hashes;
			x.Bits = Bits;
			x.Mask = Mask;
			x.MetaData = MetaData;
		}
		public BloomFilter getBloomFilter() {
			return new BloomFilter((ulong)Bits, (ulong)Hashes, Mask);
		}
	}
}