﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BloomFilterReader {
	public partial class SearchPage : ContentPage {
		private BloomFilterEntry bfe;
		public SearchPage (BloomFilterEntry sharedbfe) {
			InitializeComponent ();
			bfe = sharedbfe;
		}

		public void onTextChanged(object sender, TextChangedEventArgs args) {
			var bf = bfe.getBloomFilter();
			exists.IsToggled = bf.contains(input.Text.ToLower());
		}
	}
}

