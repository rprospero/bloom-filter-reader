﻿using System;
using System.IO;

using Xamarin.Forms;
using SQLite.Net;

namespace BloomFilterReader {
	public class ManagePage : ContentPage {
		public ManagePage (SQLiteConnection db, BloomFilter bf) {
			Title = "Manage";


			Grid grid = new Grid {
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = {
					new RowDefinition {Height = GridLength.Auto},
					new RowDefinition {Height = GridLength.Auto},
					new RowDefinition {Height = GridLength.Auto},
					new RowDefinition {Height = GridLength.Auto}},
				ColumnDefinitions = {
					new ColumnDefinition {Width = GridLength.Auto},
					new ColumnDefinition {Width = GridLength.Auto}
				}

			};

			var input = new Entry ();
			var createButton = new Button { Text = "Create" };
			var insertButton = new Button { Text = "Insert" };
			grid.Children.Add(new Label {Text = " "}, 0, 0);
			grid.Children.Add (new Label { Text = "Query" }, 0, 1);
			grid.Children.Add (input, 1, 1);
			grid.Children.Add (createButton, 0, 2);
			grid.Children.Add (insertButton, 1, 2);

			Content = grid;
			db.CreateTable<BloomFilterEntry>();
		}
	}
}


